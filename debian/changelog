rt-extension-customfieldsonupdate (1.03-3) UNRELEASED; urgency=medium

  * Remove KURASHIKI Satoru from Uploaders as requested on:
    https://alioth-lists.debian.net/pipermail/pkg-request-tracker-maintainers/2023-October/005189.html
    thank you for your work on this package.

 -- Andrew Ruthven <puck@debian.org>  Thu, 05 Oct 2023 09:53:36 +1300

rt-extension-customfieldsonupdate (1.03-2) unstable; urgency=medium

  * New Standards-Version: 4.6.2 (no changes).
  * Drop deprecated fields from d/upstream/metadata.
  * Update d/copyright.
  * Reintroduce support for request-tracker4, as we still have it in Bookworm.
  * Drop patch debianize_readme.diff and instead provide a README.Debian file.
  * Ensure we can build source after successful build.
  * Drop -common package.
  * Enable autopkgtests.
  * Upload to unstable.

 -- Andrew Ruthven <andrew@etc.gen.nz>  Mon, 28 Aug 2023 23:28:43 +1200

rt-extension-customfieldsonupdate (1.03-1) experimental; urgency=medium

  [ Andrew Ruthven ]
  * New upstream release.
  * Build for RT4 & RT5.
  * Add patch debianize_readme.diff to use Debian paths in the documentation.
  * Use debhelper compat 13
  * New Standards-Version: 4.5.1
  * Add myself and Dominic as Uploaders.
  * Depend on version of libmodule-install-rtx-perl which supports RT5.
  * Add Rules-Requires-Root: no to control.
  * Update debian/copyright.
  * Add debian/upstream/metadata.

  [ Dominic Hargreaves ]
  * Update Vcs-* fields to point to Salsa
  * Add Replaces/Breaks on earlier RT4 package
  * Drop support for request-tracker4

 -- Dominic Hargreaves <dom@earth.li>  Sat, 27 Feb 2021 12:33:03 +0000

rt-extension-customfieldsonupdate (1.02-1) unstable; urgency=medium

  * New upstream release.
  * New Standards-Version: 3.9.6
  * Depends on libmodule-install-rtx-perl.

 -- KURASHIKI Satoru <lurdan@gmail.com>  Thu, 09 Jul 2015 19:59:27 +0900

rt-extension-customfieldsonupdate (0.01-1) unstable; urgency=low

  * Initial Release. (Closes: #732665)

 -- KURASHIKI Satoru <lurdan@gmail.com>  Fri, 20 Dec 2013 11:44:01 +0900
